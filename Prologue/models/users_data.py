from db import init_mongo

BASE_FILE = 'base.json'


class UserRPG:
    def __init__(self, db_col, **kwargs):
        self.db_col = db_col
        for key in self.Meta.required_fields:
            setattr(
                self, key,
                kwargs.get(key) if kwargs.get(key)
                else self.Meta.default_values[key]
            )

    def __getitem__(self, key):
        return self.__dict__.get(key)

    def __setitem__(self, key, item):
        self.__dict__[key] = item

    @property
    def fields(self):
        return {
            key: val for key, val in self.__dict__.items()
        }

    def db_save(self):
        serialized = self.serialize()
        self.db_col.insert_one(serialized)

    def db_update(self):
        data = self.db_col.find_one({'_id': self._id})
        if data:
            serialized = self.serialize()
            self.db_col.replace_one({'_id': serialized['_id']}, serialized)

    @classmethod
    def db_find_one(cls, db_col, _id, raw=True):
        user_data = db_col.find_one({'_id': _id})
        return cls(db_col, **user_data) if user_data else None

    def serialize(self):
        result = {}
        for key in self.Meta.required_fields:
            result.update({key: self[key]})
        return result

    class Meta:
        required_fields = (
            '_id', 'name', 'save', 'save_act',
            'tempf', 'bag', 'chloc', 'characteristics','location','go_list', 'event_location',
            'RANDOM_EVENT_FLAG','EVENT_FLAG','RPG_FLAG','enemies','BATTLE_FLAG','ENEMY_TURN_FLAG',
            'PLAYER_TURN_FLAG','player_action','player','in_ch'
        )
        default_values = {
            "save": 0,
            "save_act": 0,
            "tempf": [1, BASE_FILE],
            "bag": ['Меч', 'Кинжал с золотым драконом'],
            "chloc": [0, ''],
            "characteristics": {
                'revolution': 0,
                'persAncle': 0,
                'kindness': 0,
                'persFillip': 0
            },
            "player":{
                "name": 'Жмишле',
                "HP": 200,
                "MP": 50,
                "armor": 10,
                "strength": 6,
                "agility": 4,
                "intelligence": 2,
                "actions": {
                    "Атаковать": "Вы наносите удар мечом"
                    }},
            "location":'',
            "go_list":[],
            "event_location":'',
            'RANDOM_EVENT_FLAG':0,
            'EVENT_FLAG': 0,
            'RPG_FLAG': 0,
            'enemies': [],
            'BATTLE_FLAG': 0,
            'ENEMY_TURN_FLAG': 0,
            'PLAYER_TURN_FLAG': 0,
            'player_action': '',
            'in_ch': []

        }


def get_user_data(message):
    db_col = init_mongo()
    client_data = UserRPG.db_find_one(db_col, message.chat.id, raw=False)
    if not client_data:
        client_data = UserRPG(
            db_col, _id=message.chat.id, name=message.chat.first_name
        )
        client_data.db_save()
    return client_data
