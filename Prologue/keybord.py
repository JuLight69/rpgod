from aiogram.types import ReplyKeyboardRemove, \
    ReplyKeyboardMarkup, KeyboardButton, \
    InlineKeyboardMarkup, InlineKeyboardButton
from telebot import types

def keybord(b_name):
    num = len(b_name)
    mark = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    for i in range(num):
        b = types.KeyboardButton(b_name[i])
        mark.add(b)
    return mark

def keybord_next(b_name):
    num = len(b_name)
    b = []
    mark = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    for i in range(num):
        b.append(types.KeyboardButton(b_name[i]))
    mark.row(*b)
    return mark

def keybord_battle(b_name):
    bs_name = b_name[-1:-3:-1]
    b_name = b_name[-3::-1]
    nums = len(bs_name)
    num = len(b_name)
    mark = types.ReplyKeyboardMarkup(one_time_keyboard=True, resize_keyboard=True)
    b = []
    for i in range(num):
        b = types.KeyboardButton(b_name[i])
        mark.add(b)
    b = []
    for i in range(nums):
        b.append(types.KeyboardButton(bs_name[i]))
    mark.row(*b)

    return mark
