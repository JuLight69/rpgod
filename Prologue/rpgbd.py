import sqlalchemy as sa
from sqlalchemy.ext.declarative import declarative_base
import json
from sqlalchemy.orm import sessionmaker,relationship
import os
import shutil
from datetime import datetime

Base = declarative_base()

class Common:
    id = sa.Column(sa.Integer, primary_key=True)

class BotData(Common, Base):
    __tablename__ = "RPGdata"
    obj = dict()
    chat_id = sa.Column(sa.Integer, unique=True, nullable=False )
    name = sa.Column(sa.String)
    nick = sa.Column(sa.String)
    game_class = sa.Column(sa.String)

class Guest(Common, Base):
    __tablename__ = "guests"

    name = sa.Column(sa.String(256), nullable=False)

class Role:
    def __init__(self):
        self.engine = sa.create_engine( "sqlite:///bd_opt.sqlite?check_same_thread=False")
        Session = sessionmaker()
        Session.configure(bind=self.engine)
        self.session = Session()

        Base.metadata.create_all(self.engine)

    def get_schedule(self):
        return self.session.query(Speach).all()


class SpeakerRole(Role): pass

class GuestRole(Role):
    def register(self, name):
        guest = Guest(name=name)
        self.session.add(guest)
        self.session.commit()
        self.record = guest

    def cancel(self):
        self.session.delete(self.record)
        self.session.commit()

class OrganizerRole(Role):
    def register_guest(self, name):
        guest = Guest(name=name)
        self.session.add(guest)
        self.session.commit()

    def cancel_guest(self, id):
        guest = self.session.delete(Guest).where(Guest.id == id).all()

def actBase():
    URL = "sqlite:///botdata.sqlite"
    engine = sa.create_engine(URL)
    Base.metadata.create_all(engine)
    Session = sessionmaker()
    Session.configure(bind=engine)
    session = Session()
    return session

def appendBase(session, id, fname):
    idk = (id,)
    path = os.getcwd() + f'\\users\\{id}'
    if idk not in session.query(BotData.id_name).all():
        try:
            user = BotData(chat_id=id, name=fname )
            session.add(user)
            session.commit()
        except Exception as err:
            with open('error.txt', 'a') as f:
                print(f'{fname}: ', err, f' :{datetime.today()}', file=f)
    else:
        pass
def chBase(session, id, data):
    user = session.query(User).filter_by(id)
    user.nick = data
    session.add(user)
    session.commit()
def close(session):
    session.close()
