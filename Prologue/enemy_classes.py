enemy_classes = {
    "Уродливое существо": {
        "name": 'Уродливое существо',
        "HP": 100,
        "MP": 0,
        "armor": 0,
        "strength": 5,
        "agility": 3,
        "intelligence": 0,
        "actions": {
            "attack": "Уродливое существо наносит удар"
            }
    }
}
not_targeted_actions = ["debuff"]
targeted_actions = ["Атаковать"]
