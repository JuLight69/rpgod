import telebot
from telebot import apihelper
from emoji import emojize
import os
import json
import random
from datetime import datetime

from models.users_data import BASE_FILE, get_user_data
from storys import Storys, story, marker
import keybord
from RPG import RPG
print('Start bot')

apihelper.proxy = {'https': 'socks5://278802348:49eMXKSv@orbtl.s5.opennetwork.cc:999'}
apihelper.ENABLE_MIDDLEWARE = True

token ='912782742:AAG80afwljQcnJjBINJf0BfONL_tKwumzVw'
bot = telebot.TeleBot(token)
meanU = f'https://api.telegram.org/bot{token}'
mainf = 1
back = {'уйти', 'вернуться'}


# C этим надо что-то делать, не очень понимаю почему save передавался, когда он был глобальный

@bot.middleware_handler(update_types=['message'])
def modify_message(bot_instance, message):
    try:
        message.client_data = get_user_data(message)
    except Exception as err:
        with open('error.txt', 'a') as f:
            print(f'{message.chat.first_name}: ', err,
                  f' :{datetime.today()}', file=f)


@bot.message_handler(commands=['start'])
def start_handler(message):
    lst = ['Я готов!']
    bot.send_photo(message.chat.id, open(f'./IMG/start.png', 'rb'))
    bot.send_message(
        message.chat.id,
        f''' Добро пожаловать в квест,\n ты готов начать свое путешествие? ''',
        reply_markup=keybord.keybord(lst)
    )


@bot.message_handler(content_types=['text'])
def send_handler(message):
    try:
        client_data = message.client_data
    except Exception as err:
        with open('error.txt', 'a') as f:
            print(f'{message.chat.first_name}: Нет такого id - {err}:{datetime.today()}', file=f)
            return

    if message.text == emojize(":pouch:", use_aliases=True):
        inv_str = ''
        for i in client_data.bag:
            inv_str = inv_str + i + "\n"
        sto = Storys(message.chat.id, '', '', data=client_data)
        sto.text(other=1, otxet=inv_str)

    elif message.text == emojize(":scroll:", use_aliases=True):
        stat_str = ''
        for key in client_data.characteristics:
            stat_str = stat_str + key + ': ' + str(client_data.characteristics[key]) + "\n"
        sto = Storys(message.chat.id, '', '', data=client_data)
        sto.text(other=1, otxet=stat_str)

    elif message.text == emojize(":world_map:", use_aliases=True):
        sto = Storys(message.chat.id, '', '', data=client_data)
        sto.show(other=1, otpic='map0.jpg')

    elif client_data.RPG_FLAG:
        rpg = RPG( message.chat.id, message.text, data=client_data)
        rpg.eventManager()

    else:
        with open('loggg.txt', 'a') as target:
            print('story', file = target)
        story(client_data, message)


if __name__ == '__main__':
    bot.polling()
