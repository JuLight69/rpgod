import requests
import telebot
from telebot import types
import rpgbd
from datetime import datetime
import keybord
import time
from telebot import apihelper

apihelper.proxy = {'https': 'socks5://278802348:49eMXKSv@orbtl.s5.opennetwork.cc:999'}

token ='912782742:AAG80afwljQcnJjBINJf0BfONL_tKwumzVw'
bot = telebot.TeleBot(token)
meanU = f'https://api.telegram.org/bot{token}'
global save, save_act, mainf
mainf = 1
save = {}
save_act = {}
back = set(['уйти', 'вернуться'])

class Storys:
    def __init__(self, messageid, text, str):
        self.mid = messageid
        self.txt = text
        self.data = str

    def text(self):
        bot.send_message(self.mid, self.data, reply_markup=keybord.keybord(['next']))

    def enter(self):
            nowm = self.txt
            session = rpgbd.actBase()
            bot.send_message(self.mid, self.data)

    def choise(self):
        self.data = self.data.split('#')
        ch = self.data[1:]
        bot.send_message(self.mid, self.data[0], reply_markup=keybord.keybord(ch))

    def show(self):
        bot.send_photo(self.mid, open(f'./IMG/{self.data}', 'rb'), reply_markup=keybord.keybord(['next']))

    def action(self):
        mark = self.data.split('_')
        def data_wr(name, val):
            print(f'create in bd {name} is {val} ')
            bot.send_message(self.mid,'Хорошо.', reply_markup=keybord.keybord(['next']))

        def file_wk(namef, val):
            if val in back:
                with open(f'Main/{namef[1]}-{val}.txt', 'r', encoding='utf-8') as full:
                    marker(full, self.mid, self.txt, save_act)
            else:
                with open(f'Main/{namef[0]}-{val}.txt', 'r', encoding='utf-8') as full:
                    marker(full, self.mid, self.txt,save_act)
            save_act [self.mid] = 0

        if mark[0]=='wr':
            data_wr(mark[1], self.txt)
        elif mark[0]=='fn':
            file_wk(mark[1:], self.txt)

def marker(f, messageid,text,save):
    f.seek(save[messageid])
    line = f.readline()
    pos = f.tell()
    save[messageid] = pos
    base = line.split('*')
    mk = base[0]
    print("BASE",base)
    print(save)
    sto = Storys(messageid, text, base[1])
    if mk == 't':
        sto.text()
    elif mk == 'p':
        sto.enter()
    elif mk == 'ch':
        sto.choise()
    elif mk == 'def':
        sto.action()
    elif mk == 'ignore':
        pass
    elif mk == 'img':
        sto.show()
    else:
        print('error')

@bot.message_handler(commands=['start'])
def start_handler(message):
    session = rpgbd.actBase()
    name = message.chat.first_name
    save[message.chat.id] = 0
    save_act [message.chat.id] = 0
    try:
        rpgbd.appendBase(session, message.chat.id, message.chat.first_name)
    except Exception as err:
        with open('error.txt', 'a') as f:
            print(f'{name}: ', err, f' :{datetime.today()}', file=f)
    lst = ['Я готов!']
    bot.send_message(message.chat.id, f'''Добро пожаловать в квест, ты готов начать свое путешествие? ''', reply_markup=keybord.keybord(lst))
    rpgbd.close(session)

@bot.message_handler(content_types=['text'])
def send_handler(message):
    def story():
        with open('basesript.txt', 'r', encoding='utf-8') as f:
            marker(f, message.chat.id, message.text, save)
    if message.text:
        story()

if __name__ == '__main__':
     bot.polling()
