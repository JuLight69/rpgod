FROM python:3.8-buster

COPY ./Skaramush_json/requirements.txt /code/
RUN pip install -r /code/requirements.txt

COPY ./Prologue /code/

WORKDIR /code
