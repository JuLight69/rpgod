import telebot
import json
from emoji import emojize
import random
import keybord
from models.users_data import BASE_FILE, get_user_data
import RPG

back = ["Назад", "Уйти"]

def story(client_data, message,bot):
    with open(client_data.tempf[1],  encoding='utf-8') as f:
        f = json.load(f)
        if client_data.tempf[0] == 0:
            marker(f, message.chat.id, message.text, client_data,bot,message,save=False)
        elif client_data.tempf[0] == 1:
            marker(f, message.chat.id, message.text, client_data,bot,message, save=True)
        else:
            print('main error')

def marker(f, messageid, text, client_data,bot,message,save=True):
    if save:
        actual_save = client_data.save
        client_data.save = client_data.save + 1
    else:
        actual_save = client_data.save_act
        client_data.save_act = client_data.save_act + 1
    def change_file():
        client_data.save_act = 0
        client_data.tempf[0] = 1
        client_data.tempf[1] = BASE_FILE

    def go_to_f(f, folder=''):
        client_data.tempf[1] = f'Main/{folder}{f}.json'
        client_data.save_act = 0
        client_data.tempf[0] = 0
        client_data.db_update()
        print(client_data.tempf)
        sto.action(mark, client_data.tempf[1])

    #print(client_data.tempf, client_data.save, client_data.save_act,actual_save,save)
    obj = f[str(actual_save)]

    mk = obj.get('type')
    sto = Storys(messageid, text, obj.get('data'),bot,message, data=client_data)
    if mk == 't':
        sto.text()
    elif mk == 'p':
        sto.enter()
    elif mk == 'ch':
        sto.choise()
    elif mk == 'rpg':
        rpg = RPG.RPG( messageid,text,bot,message,data=client_data)
        client_data.location = obj.get('location')
        client_data.go_list = obj.get('golist')
        client_data.RANDOM_EVENT_FLAG = 0
        client_data.EVENT_FLAG = 0
        client_data.RPG_FLAG = 1
        client_data.db_update()
        rpg.eventManager()

    elif mk == 'def':
        mark = obj.get('mark')
        if mark == 'fn':
            client_data.tempf[0] = 0
            namef = obj.get('data')
            folder = obj.get('folder')
            #если  obj.get('return') 1 ret есть и нам нужно вернуться на файл назад
            if text in back:
                if not obj.get('return'):
                    change_file()
                    sto.action(mark, client_data.tempf[1])
                else:
                    go_to_f(obj.get('return'), folder=folder + "/")
            else:
                for i, n in enumerate(client_data.in_ch):
                    if n == text:
                        go_to_f(f'{namef}-{i+1}', folder=folder + "/")

        elif mark == 'spch':
            client_data.tempf[0] = 0
            namef = obj.get('data')
            client_data.tempf[1] = f'Main/{namef}.json'
            client_data.save_act = 0
            with open(client_data.tempf[1], encoding='utf-8') as fu:
                fu = json.load(fu)
                act = fu[text]
                sto.text(other=1, otxet=act.get('data'))
                stat = act.get('stat')
                for key in stat:
                    n = stat[key]
                    client_data.characteristics[key] = client_data.characteristics[key] + int(n)
                change_file()

    elif mk == 'ignore':
        pass
    elif mk == 'img':
        sto.show()
    elif mk == 'end':
        rpg = RPG.RPG( messageid,'end',bot,message, data=client_data)
        rpg.eventManager()
    elif 'battle':
        sto.text()
        rpg = RPG.RPG( messageid,obj.get('enemies'),bot,message, data=client_data)
        rpg.battleManager()
    else:
        print('error')

    client_data.db_update()


class Storys:
    def __init__(self, messageid, text, str,bot,message,data):
        self.mid = messageid
        self.message = message
        self.txt = text
        self.bot = bot
        self.dataB = data
        self.next = [
            emojize(":scroll:", use_aliases=True),
            emojize(":pouch:", use_aliases=True),
            emojize(":world_map:", use_aliases=True),
            emojize(":arrow_right:", use_aliases=True),
        ]
        self.fightk = ["хуита стаса",emojize(":scroll:", use_aliases=True),
                            emojize(":pouch:", use_aliases=True)]
        self.data = str

    def text(self, other=0, otxet=''):
        if not other:
            self.bot.send_message(
                self.mid, self.data,
                reply_markup=keybord.keybord_next(self.next))
        else:
            #if self.dataB.PLAYER_TURN_FLAG:
            #    self.bot.send_message(self.mid, otxet,
            #                    reply_markup=keybord.keybord_next(self.fightk))
            #else:
            self.bot.send_message(self.mid, otxet,
                                reply_markup=keybord.keybord_next(self.next))

    def enter(self):
            nowm = self.txt
            self.bot.send_message(self.mid, self.data)

    def choise(self):
        self.dataB.in_ch = self.data.get('answer')
        self.bot.send_message(self.mid, self.data.get('data'),
                                reply_markup=keybord.keybord(self.data.get('answer')))
    def show(self, other=0, otpic=''):
        if not other:
            self.bot.send_photo(
                self.mid, open(f'./IMG/{self.data}', 'rb'),
                reply_markup=keybord.keybord_next(self.next))
        else:
            self.bot.send_photo(
                self.mid, open(f'./IMG/{otpic}', 'rb'),
                reply_markup=keybord.keybord_next(self.next))

    def action(self, mark, temp):
        def data_wr(name, val):
            self.bot.send_message(
                self.mid,'Хорошо.',
                reply_markup=keybord.keybord_next(self.next))

        def file_wk(namef, val):
            with open(temp, encoding='utf-8') as full:
                full = json.load(full)
                if self.dataB.tempf[0]:
                    marker(full, self.mid, self.txt, self.dataB,self.bot,self.message)
                else:
                    marker(full, self.mid, self.txt, self.dataB,self.bot,self.message, save=False)

        def spch(namef, val):
            with open(temp, encoding='utf-8') as full:
                full = json.load(full)
                marker(full, self.mid, self.txt, self.dataB,self.bot,self.message, save=False)
        if mark == 'wr':
            data_wr(mark[1], self.txt)
        elif mark == 'fn':
            file_wk(mark[1:], self.txt)
        elif mark == 'spch':
            spch(self.data, self.txt)
