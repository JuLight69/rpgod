enemy_classes = {
    "dog": {
        "name": 'dog',
        "HP": 100,
        "MP": 0,
        "armor": 0,
        "strength": 3,
        "agility": 4,
        "intelligence": 0,
        "actions": {
            "attack": "Собака кусает"
            }
    },
    "ded": {
        "name": 'ded',
        "HP": 150,
        "MP": 10,
        "armor": 0,
        "strength": 2,
        "agility": 1,
        "intelligence": 2,
        "actions": {
            "attack": "Дед наносит удар",
            "debuff": "Дед проклинает вас"
            }
    },
    "boss": {
        "name": 'boss',
        "HP": 200,
        "MP": 100,
        "armor": 4,
        "strength": 5,
        "agility": 4,
        "intelligence": 4,
        "actions": {
            "attack": "Бос отчитывает вас",
            "spell": "Бос использует заклинание 'отмена премии'"}
    }
}
not_targeted_actions = ["debuff"]
targeted_actions = ["Атаковать"]
