from pymongo import MongoClient


def init_mongo():
    client = MongoClient('mongo', 27017)
    db = client.rpg_db
    client_data = db.client_data
    return client_data
