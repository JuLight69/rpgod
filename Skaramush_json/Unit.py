import random

class Unit:
    def __init__(self, name, HP, MP, armor, strength, agility, intelligence,actions):
        self.name = name
        self.HP = HP
        self.MP = MP
        self.armor = armor
        self.strength = strength
        self.agility = agility
        self.intelligence = intelligence
        self.actions = actions

    def checkEvasion(self):
        max_chance = 100
        evade_chance = 1 * self.agility
        return random.choices([1, 0], weights = [max_chance - evade_chance,evade_chance], k=1)
#НУЖЕН ФАЙЛ С ОРУЖИЕМ + РАЗНЫЙ ДАМАГ
    def setDamage(self, weapon):
        if weapon == "None":
            raw_damage = 37
        elif weapon["type"] == "sword" or "mace":
            raw_damage = weapon["damage"] + (self.strength * weapon["damage"] // 100)
        elif weapon["type"] == "bow":
            raw_damage = weapon["damage"] + (self.agility * weapon["damage"] // 100)
        elif weapon["type"] == "staff":
            raw_damage = weapon["damage"] + (self.intelligence * weapon["damage"] // 100)
        else :
            raw_damage = 0
        return raw_damage

    def getDamage(self, raw_damage):
        damage_reduction = 0.1 * self.armor
        print(self.checkEvasion(),'govnyani choice')
        damage = (raw_damage - damage_reduction) * int(self.checkEvasion()[0])
        if damage < 0:
            damage = 0
        return damage

    def changeHP(self, damage = 0, heal = 0):
        self.HP = self.HP - damage + heal

    def changeMP(self, damage = 0, heal = 0):
        self.MP = self.MP - damage + heal

    def createDict(self):
        unitDict = {}
        keylist = ['name','HP', 'MP', 'armor', 'strength',
                                    'agility', 'intelligence','actions']
        valuelist = [self.name, self.HP, self.MP, self.armor, self.strength,
                                    self.agility, self.intelligence, self.actions]
        for i,n in enumerate(keylist):
            unitDict[n] = valuelist[i]

        return unitDict
