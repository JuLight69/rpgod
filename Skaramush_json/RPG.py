import telebot
import random
import json
import os
from emoji import emojize

from Unit import Unit
from models.users_data import BASE_FILE, get_user_data
import storys
import keybord
from enemy_classes import enemy_classes,not_targeted_actions,targeted_actions
#ПРОДОЛЖИТЬ
class RPG:
    def __init__(self, messageid, event_info,bot,message, data):
        self.bot = bot
        self.message = message
        self.mid = messageid # чат, шлем все туда
        self.loc = data.location # большая локация, строка, название локи
        self.data = data # какая-то хрень(типо кусок текста)((кажется это вся бд для юзера))
        self.go = data.go_list  # мини лока, хожу куда хочу, список строк с именами в большой локе(много папок)
        #self.go.append("Вернуться к истории")
        self.path = f'./RPG/{self.loc}' # путь к локейшн
        self.event_info = event_info # инфа из дока о событии(рандомном, там, или не рандомном)((текст, данные))
        self.not_targeted_actions = not_targeted_actions
        self.targeted_actions = targeted_actions
        self.next = [
            emojize(":scroll:", use_aliases=True),
            emojize(":pouch:", use_aliases=True),
            emojize(":world_map:", use_aliases=True),
            emojize(":arrow_right:", use_aliases=True),]
        act = self.data.player['actions'].keys()
        self.actions_key =[ *act, "Враги", emojize(":pouch:", use_aliases=True),
         emojize(":hearts:", use_aliases=True)]


# ответ куда отправиться - текст
    def chooseLoc(self): # получить куда игрок хочет пойти по клавиатуре(запрос)
        self.bot.send_message(
            self.mid, "Куда отправиться?",
            reply_markup=keybord.keybord(self.go)
        )# + вернуться назад
        # подумоть как зищититься от ввода рандомного говна
        # 1- значит локация изменена и ее надо считать в выборе, 0 -локация не изменена текущая указа в индексе 1
        # подняли члок - не забыли уронить(где-то)
        #self.data.chloc[0] = 1 #хз откуда, что-то от базы данных(маркер говорит о том, что надо чекнуть текст на отправление)
        #self.data.db_update()

# получили ответ от юзера на запрос и пошли туда(в папку)
    def changeLoc(self):
        return f'{self.path}/{self.data.event_location}'

    def generateEvent(self):
        DIR = os.path.join(self.changeLoc(),'random_events')
        namef = os.path.join(DIR, random.choice(os.listdir(DIR)))
        self.data.save_act = 0
        self.data.tempf[0] = 0
        self.data.tempf[1] = namef
        return namef

    def randomEventStart(self):
        self.data.event_location = self.event_info
        self.data.event = self.generateEvent()
        self.data.RANDOM_EVENT_FLAG = 1
        self.data.db_update()
        self.bot.send_message(
            self.mid, "Вы отправляетесь далее",
            reply_markup=keybord.keybord_next(self.next))

    def eventManager(self):
        if self.event_info == "Вернуться к истории":
            self.data.RPG_FLAG = 0
            self.data.RANDOM_EVENT_FLAG = 0
            self.data.EVENT_FLAG = 0
            self.data.tempf[0] = 1
            self.data.tempf[1] = BASE_FILE
            self.data.db_update()
            self.bot.send_message(self.mid, "Вы решили, что закончили свои дела, и возвращаетесь.",
                reply_markup=keybord.keybord_next(self.next))
        elif not self.data.EVENT_FLAG:
            if not self.data.RANDOM_EVENT_FLAG:
                if self.event_info not in self.go:
                    self.chooseLoc()
                else:
                    self.randomEventStart()
            elif self.event_info == "end":
                self.data.RANDOM_EVENT_FLAG = 0
                self.data.EVENT_FLAG = 1
                self.data.db_update()
                self.bot.send_message(self.mid, "Вы отправляетесь далее.",
                    reply_markup=keybord.keybord_next(self.next))
            else:
                storys.story(self.data, self.message, self.bot)
        else:
            path = os.path.join(self.changeLoc(), "location_events")
            location_events = os.listdir(path) # tut путь к обязательным ивенам
            event_counter = 0
            if location_events:
                self.data.event = location_events[event_counter] # tut
                storys.story(self.data, self.message, self.bot)
            elif self.event_info == "end":
                self.data.EVENT_FLAG = 0
                self.data.db_update()
                self.bot.send_message(self.mid, "Вы отправляетесь далее.",
                    reply_markup=keybord.keybord_next(self.next))
            else:
                self.data.EVENT_FLAG = 0
                self.data.db_update()
                self.bot.send_message(self.mid, "Вы отправляетесь далее.",
                    reply_markup=keybord.keybord_next(self.next))

    def battleManager(self):
        print(self.event_info,self.targeted_actions,'BM')
        if not self.data.BATTLE_FLAG:
            self.battleStart()
        elif self.data.ENEMY_TURN_FLAG:
            self.bot.send_message(self.mid, "Очередь хода врагов.")
            self.enemyManager()
        elif self.data.PLAYER_TURN_FLAG:
            if self.data.player_action:
                self.playerAction(action = self.data.player_action, target = self.event_info)
            elif self.event_info == "Враги":
                self.getEnemiesInfo()
            elif self.event_info in self.targeted_actions or self.event_info in self.not_targeted_actions:
                print(self.event_info,"STASU ZHALKO")
                self.data.player_action = self.event_info
                self.data.db_update()
                self.playerAction(action = self.data.player_action)#<------------------------------------------------------------
            else: # это надо как-то по другому встраивать в бота, потому что после первого инвенторя он станет другую клаву кидать
                self.bot.send_message(
                    self.mid, "Ваша очередь. Что вы будете делать?",
                    reply_markup=keybord.keybord_battle(self.actions_key )
                )

# Создать сколько-то противников, информация о кол-ве лежит в файле.
# Если вместо кол-ва "random", кол-во противников будет рандомным
# в диапазоне 1 : max_enemy_quantity.
# Классы врагов лежат в enemy_classes.py
    def generateEnemy(self):
        max_enemy_quantity = 5
        for i in self.event_info:
            enemy_quantity  =  self.event_info[i]
            enemy_class = enemy_classes[i]
            if enemy_quantity == "random":
                enemy_quantity = random.randint(1, max_enemy_quantity)
            enemies = []
            for i in range(0,enemy_quantity):
                enemies.append(enemy_class)
            return enemies

# Выгрузить данные врагов в сообщение NO TUT
    def getEnemiesInfo(self):
        for i in self.data.enemies:
            self.bot.send_message(
            self.mid, f'''{i['name']} - Здоровье: {int(i['HP'])} Броня: {i['armor']} Мана: {i['MP']}
Сила: {i['strength']} Ловкость: {i['agility']} Интеллект: {i['intelligence']}''')

    def battleStart(self):
        self.data.BATTLE_FLAG = 1
        self.data.enemies = self.generateEnemy()
        self.data.ENEMY_TURN_FLAG = 1 # Нужно бужет эти флаги объединить в один потом
        self.data.PLAYER_TURN_FLAG = 0
        self.data.db_update()
        self.bot.send_message(
            self.mid, ' Битва началась.',
                reply_markup=keybord.keybord_next(self.next))

    def createUnit(self):
        player = Unit(**self.data.player)
        enemies_list = []
        for i in self.data.enemies:
            enemy = Unit(**i)
            enemies_list.append(enemy)
        return player, enemies_list

    def rewriteUnit(self, player, enemies):
        player = player.createDict()
        new_enemies = []
        for i in enemies:
            enemy = i.createDict()
            new_enemies.append(enemy)
        self.data.enemies = new_enemies
        self.data.player = player
        self.data.db_update()

    def playerAction(self, action, target = ""):
        print('playerAction',self.event_info)
        player, enemies = self.createUnit()
        if action in self.not_targeted_actions:# tut
            if action == "Ждать":
                self.bot.send_message(
                    self.mid, "Вы добровольно пропускаете ход"
                )
                self.data.ENEMY_TURN_FLAG = 1#tut?
                self.data.PLAYER_TURN_FLAG = 0#tut?
                self.rewriteUnit(player, enemies)
                self.data.db_update()
            else:
                pass
                #playerAction unexpected value action
        elif action in self.targeted_actions:# tut
            if  len(enemies) == 1:
                print(len(enemies),'enemies')
                target = enemies[0]
            elif target == "":
                #составить список врагов#tut
                enemy_list =[]
                for i in enemies:
                    enemy_list.append(enemies.name)# добавить цифры для разных врагов
                self.bot.send_message(
                    self.mid, "Выберите цель.",
                    reply_markup=keybord.keybord(enemy_list)
                )
            if action == "Атаковать":
                for i in enemies:
                    if target == i.name:
                        target = i
                raw_damage = player.setDamage(weapon = 'None')#класс игрока удаляетсяи каждый ход
                damage = target.getDamage(raw_damage = raw_damage)
                target.changeHP(damage = damage)
                self.rewriteUnit(player, enemies)
                self.data.db_update()
                self.bot.send_message(
                    self.mid, f'Вы наносите удар врагу {target.name}. Урон здоровью:{damage}.'
                )
                self.bot.send_message(
                    self.mid, f'Здоровье:{target.HP}. Мана:{target.MP}.'
                )
            else:
                print('playerAction unexpected value action')
                pass
                #playerAction unexpected value action
        else:
            print('attack error')
            pass
            #смэрт # tut
        self.data.ENEMY_TURN_FLAG = 1
        self.data.PLAYER_TURN_FLAG = 0
        self.data.player_action = ""
        self.rewriteUnit(player, enemies)
        self.data.db_update()
        self.bot.send_message(
            self.mid, "Ход завершен.",
            reply_markup=keybord.keybord_next(self.next)
        )

    def enemyAction(self, enemy):
        player,enemies = self.createUnit()
        #keylist = enemy.actions.keys()
        #action = random.choice(keylist)
        action = "attack"
        if action == "attack":
            raw_damage = enemy.setDamage("None") # ADD WEAPON INSTEAD NONE
            damage = player.getDamage(raw_damage = raw_damage)
            player.changeHP(damage = damage)#меняется ли хп в дб
            self.rewriteUnit(player, enemies)
            self.data.db_update()
            self.bot.send_message(
                self.mid, f'{action}. Урон здоровью:{damage}.'
            )
            self.bot.send_message(
                self.mid, f'Здоровье:{player.HP}. Мана:{player.MP}.'
            )
        elif action == "debuff":
            player.changeStat()
            self.rewriteUnit(player, enemies)
            self.data.db_update()
            self.bot.send_message(
                self.mid, f'''{action}. Сила:{player.strength}.
                 Ловкость:{player.agility}. Интеллект:{player.intelligence}.''')

        elif action == "spell":
            raw_spell_damage = enemy.setSpellDamage(tut)#tut??
            spell_damage = player.getSpellDamage(raw_spell_damage = raw_spell_damage)
            player.changeHP(damage = spell_damage)
            self.rewriteUnit(player, enemies)
            self.data.db_update()
            self.bot.send_message(
                self.mid, f'{action}. Урон здоровью:{damage}.'
            )
            self.bot.send_message(
                self.mid, f'Здоровье:{player.HP}. Мана:{player.MP}.'
            )
        else:
            print('enemyAction unexpected value action')
            pass
            #enemyAction unexpected value action

# Делает всех врагов сходить и чекает кто сдох
    def enemyManager(self):
        player,enemies = self.createUnit()
        for i in range(0, len(enemies)):
            if enemies[i].HP <= 0:
                del enemies[i]
        self.rewriteUnit(player, enemies)
        self.data.db_update()
        if self.data.enemies == []:
            self.battleEnd()
        for i in range(0, len(enemies)):
            self.enemyAction(enemies[i])
        self.data.ENEMY_TURN_FLAG = 0
        self.data.PLAYER_TURN_FLAG = 1
        #self.rewriteUnit(player, enemies)
        self.data.db_update()
        self.bot.send_message(
            self.mid, "Ход врагов завершен.",
            reply_markup=keybord.keybord_next(self.next)
        )


    def battleEnd(self):# мб не понадобится
        self.data.BATTLE_FLAG = 0
        self.data.db_update()
