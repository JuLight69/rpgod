class UserRPG:
    def __init__(self, db_col, **kwargs):
        self.db_col = db_col
        for key in self.Meta.required_fields:
            setattr(
                self, key,
                kwargs.get(key) if kwargs.get(key)
                else self.Meta.default_values[key]
            )

    def __getitem__(self, key):
        return self.__dict__.get(key)

    def __setitem__(self, key, item):
        self.__dict__[key] = item

    def db_save(self):
        serialized = self.serialize()
        self.db_col.insert_one(serialized)

    def db_update(self):
        data = self.db_col.find_one({'id': self.id})
        if data:
            serialized = self.serialize()
            self.db_col.replace_one({'id': serialized['id']}, serialized)

    @classmethod
    def db_find_one(cls, db_col, user_data):
        return cls(db_col, **user_data)

    def serialize(self):
        return {
            key: val for key, val in self.__dict__.items()
        }

    class Meta:
        required_fields = (
            'id', 'name', 'save', 'save_act',
            'tempf', 'bag', 'chloc', 'characteristics'
        )
        default_values = {
            "save": 0,
            "save_act": 0,
            "tempf": [1, 2],
            "bag": ['100 монет', 'Свод законов'],
            "chloc": [0, ''],
            "characteristics": {
                'revolution': 0,
                'persAncle': 0,
                'kindness': 0,
                'persFillip': 0
            }
        }

db_col = 0
id = 1

data_from_db = {
    "id": 1,
    "name": "first_name",
    "save": 0,
    "save_act": 0,
    "tempf": [1, 2],
    "bag": ['100 монет', 'Свод законов'],
    "chloc": [0, ''],
    "characteristics": {
        'revolution': 0,
        'persAncle': 0,
        'kindness': 0,
        'persFillip': 0
    }
}

client_data = UserRPG.db_find_one(db_col, data_from_db)

print(client_data.serialize())